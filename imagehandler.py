import time
import http.client, urllib.request, urllib.parse, urllib.error, base64
import json
import math
import matplotlib.pyplot as plt
import cv2
from takepic import takepic

class ImageHandler:
    def __init__(self, use_opencv=False, camera_id=0):
        self.theta_x = 2*math.atan(1.95/4.1) # Horizontal FOV of camera
        self.theta_y = 2*math.atan(1.2/4.1) # Vertical FOV of camera
        self.watergun_velocity = 50. # Velocity of the water gun
        self.offset_y = 0.1 # How far the water gun is above the webcam, in metres
        self.offset_x = 0.3 # How far the water gun is behind the webcam, in metres

        self.use_opencv = use_opencv
        if use_opencv:
            self.cam = cv2.VideoCapture(camera_id)
            while not self.cam.isOpened():
                time.sleep(0.1)
            test_img = self.take_picture()
            self.height, self.width = test_img.shape[:2]
        else:
            self.height, self.width = 480, 640

        self.face_url = 'uksouth.api.cognitive.microsoft.com'
        self.face_headers = {
            # Request headers
            'Content-Type': 'application/octet-stream',
            'Ocp-Apim-Subscription-Key': '730bfce00fa742e7ba62c47ce1d43fb2',
        }
        self.face_params = urllib.parse.urlencode({
            # Request parameters
            'returnFaceId': 'true',
            'returnFaceLandmarks': 'true',
            #'returnFaceAttributes': '{string}',
        })

    # Given image data in body, sends a request to the microsoft face API and returns face positions and features
    def request_face(self, body):
        try:
            conn = http.client.HTTPSConnection(self.face_url)
            conn.request("POST", "/face/v1.0/detect?%s" % self.face_params, body, self.face_headers)
            response = conn.getresponse()
            data = json.loads(response.read())
            conn.close()
            return data
        except Exception as e:
            print("[Errno {0}] {1}".format(e.errno, e.strerror))

    # Takes a webcam picture
    def take_picture(self):
        for i in range(10):
            self.cam.read()
        ret, img = self.cam.read()
        return img


    # Finds the intersection points of the lines (p1, p2) and (p3, p4)
    def intersect(self, p1, p2, p3, p4):
        (x1, y1) = p1
        (x2, y2) = p2
        (x3, y3) = p3
        (x4, y4) = p4

        u1 = ((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3))
        u2 = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1))
        u = u1 / u2

        x = x1 + (u * (x2 - x1))
        y = y1 + (u * (y2 - y1))

        return (int(x),int(y))

    # Given data returned from the face API, parses it into a more useful form
    def get_points(self, data):
        points = []
        for face in data:
            face_x = face['faceRectangle']['left']
            face_y = face['faceRectangle']['top']
            face_w = face['faceRectangle']['width']
            face_h = face['faceRectangle']['height']
            mouth_x1 = int(face['faceLandmarks']['mouthLeft']['x'])
            mouth_y1 = int(face['faceLandmarks']['mouthLeft']['y'])
            mouth_x2 = int(face['faceLandmarks']['mouthRight']['x'])
            mouth_y2 = int(face['faceLandmarks']['mouthRight']['y'])
            lips_x1 = int(face['faceLandmarks']['upperLipTop']['x'])
            lips_y1 = int(face['faceLandmarks']['upperLipTop']['y'])
            lips_x2 = int(face['faceLandmarks']['underLipBottom']['x'])
            lips_y2 = int(face['faceLandmarks']['underLipBottom']['y'])
            eyes_x1 = int((face['faceLandmarks']['eyeLeftOuter']['x'] + face['faceLandmarks']['eyeLeftInner']['x']) / 2)
            eyes_y1 = int((face['faceLandmarks']['eyeLeftOuter']['y'] + face['faceLandmarks']['eyeLeftInner']['y']) / 2)
            eyes_x2 = int((face['faceLandmarks']['eyeRightOuter']['x'] + face['faceLandmarks']['eyeRightInner']['x']) / 2)
            eyes_y2 = int((face['faceLandmarks']['eyeRightOuter']['y'] + face['faceLandmarks']['eyeRightInner']['y']) / 2)

            centre = self.intersect((mouth_x1, mouth_y1), (mouth_x2, mouth_y2), (lips_x1, lips_y1), (lips_x2, lips_y2))
            openness = (math.sqrt((lips_x2 - lips_x1)**2 + (lips_y2 - lips_y1)**2) * 100) / face_h
            ipd = math.sqrt((eyes_x2 - eyes_x1)**2 + (eyes_y2 - eyes_y1)**2)

            points_i = dict(face=[(face_x, face_y), (face_x+face_w, face_y+face_h)],
                    mouth=[(mouth_x1, mouth_y1), (mouth_x2, mouth_y2)],
                    lips=[(lips_x1, lips_y1), (lips_x2, lips_y2)],
                    eyes=[(eyes_x1, eyes_y1), (eyes_x2, eyes_y2)],
                    centre=centre,
                    openness=openness,
                    ipd=ipd)
            points.append(points_i)
        return points

    # Given an image and raw data from the face API, draws rectangles and lines over the features
    def draw_rects(self, img, data):
        points = self.get_points(data)
        img2 = img
        for i in range(len(data)):
            # Draw rectangle around face
            img2 = cv2.rectangle(img2, points[i]['face'][0], points[i]['face'][1], (255,0,0))

            # Draw horizontal mouth line
            img2 = cv2.line(img2, points[i]['mouth'][0], points[i]['mouth'][1], (0,255,0))

            # Draw vertical mouth line
            img2 = cv2.line(img2, points[i]['lips'][0], points[i]['lips'][1], (0,255,0))

            # Draw eye line
            img2 = cv2.line(img2, points[i]['eyes'][0], points[i]['eyes'][1], (0,255,0))

            # Draw point at centre of mouth
            img2 = cv2.circle(img2, points[i]['centre'], 5, (0,255,0), thickness=-1)

            # Draw "openness" of mouth as number
            img2 = cv2.putText(img2, str(points[i]['openness']), points[i]['face'][0], cv2.FONT_HERSHEY_PLAIN, 1.0, (255,0,0))

            # Draw ipd as number
            img2 = cv2.putText(img2, str(points[i]['ipd']), (points[i]['face'][0][0], points[i]['face'][1][1]+12), cv2.FONT_HERSHEY_PLAIN, 1.0, (255,0,0))

        return img2

    # Given x, y, width, height, ipd, return (yaw, pitch)
    def get_yaw_pitch(self, x, y, width, height, ipd):
        average_ipd = 0.0629
        angular_separation = (ipd / float(width)) * self.theta_x
        distance = average_ipd / (2 * math.tan(angular_separation / 2))

        yaw = ((float(x)/float(width)) * 2) - 1 # value between -1 and 1, where the far edges are the edge of the FOV

        v_height = 2 * distance * math.tan(self.theta_y / 2)
        v_distance = 0.
        v_distance = ((float(y)/float(height)) * v_height * (-1.)) - (v_height / 2)
        (x_relative, y_relative) = (distance + self.offset_x, v_distance - self.offset_y)
        pitch = 0.
        pitch = math.atan(y_relative/x_relative) + 0.4 * distance
        #pitch = v_distance / v_height
        #v_distance = 2 * distance * np.tan(self.theta_y / 2)
        #z_ = ((float(y)/float(height)) * v_distance * (-1.)) - (v_distance / 2)
        #x_ = distance
        #alpha = (9.81/(self.watergun_velocity ** 2)) + z_
        #num = (alpha * x_) - (z_ * np.sqrt((z_ ** 2) + (x_ ** 2) - (alpha ** 2)))
        #denom = (z_ ** 2) + (x_ ** 2)
        #pitch = np.arcsin(num / denom) / 2

        return (yaw, pitch)

    # Take picture and run processing to return (yaw, pitch) of first face found
    def run_processing(self, show=False):
        done = False
        while not done:
            if self.use_opencv:
                img = self.take_picture()
                img_str = cv2.imencode('.jpg', img)[1].tostring()
            else:
                takepic()
                with open("output.jpg", "rb") as imageFile:
                    f = imageFile.read()
                    img_str = bytearray(f)
            data = self.request_face(img_str)
            error = None if isinstance(data, list) else data.get('error')
            if error is not None:
                print("Error: {}".format(error))
            done = (data is not None and len(data) > 0 and error is None)
            if not done:
                print("Get in the heckin picture!")
                time.sleep(1)

        points = self.get_points(data)
        #print(points)

        if show:
            img2 = self.draw_rects(img, data)
            plt.imshow(img2)
            plt.show()

        return self.get_yaw_pitch(points[0]['centre'][0], points[0]['centre'][1], self.width, self.height, points[0]['ipd'])


