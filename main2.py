from imagehandler import ImageHandler
from sendtoserver import shootmeupdaddy
from playsound import playsound
import random
import os
from subprocess import call

files = os.listdir("turretsounds")
active = list(filter(lambda x: x.startswith("turret_active"), files))
autosearch = list(filter(lambda x: x.startswith("turret_autosearch"), files))
disabled = list(filter(lambda x: x.startswith("turret_disabled"), files))

imgh = ImageHandler(use_opencv=True, camera_id=2)

try:
    while True:
        text = input("Press enter for SHOTS SHOTS SHOTS")
        show = (len(text) > 0)
        try:
            (yaw, pitch) = imgh.run_processing(show=show)
            call(["aplay", "turretsounds/"+random.choice(active)])
        except Exception as e:
            print(e)
        else:
            shootmeupdaddy(yaw, pitch, True)
            call(["aplay", "turretsounds/"+random.choice(autosearch)])
except KeyboardInterrupt:
    call(["aplay", "turretsounds/"+random.choice(disabled)])
