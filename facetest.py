import time
import http.client, urllib.request, urllib.parse, urllib.error, base64
import json
import cv2
import numpy as np
import matplotlib.pyplot as plt

# FOV of camera
theta = 2*np.arctan(1.95/4.1)

cam = cv2.VideoCapture(0)
while not cam.isOpened():
    time.sleep(0.1)

face_url = 'uksouth.api.cognitive.microsoft.com'
face_headers = {
    # Request headers
    'Content-Type': 'application/octet-stream',
    'Ocp-Apim-Subscription-Key': '730bfce00fa742e7ba62c47ce1d43fb2',
}
face_params = urllib.parse.urlencode({
    # Request parameters
    'returnFaceId': 'true',
    'returnFaceLandmarks': 'true',
    #'returnFaceAttributes': '{string}',
})

data = {
}

def request_face(body):
    try:
        conn = http.client.HTTPSConnection(face_url)
        conn.request("POST", "/face/v1.0/detect?%s" % face_params, body, face_headers)
        response = conn.getresponse()
        data = json.loads(response.read())
        conn.close()
        return data
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))

def take_picture():
    for i in range(10):
        cam.read()
    ret, img = cam.read()
    return img

# Finds the intersection points of the lines (p1, p2) and (p3, p4)
def intersect(p1, p2, p3, p4):
    (x1, y1) = p1
    (x2, y2) = p2
    (x3, y3) = p3
    (x4, y4) = p4

    u1 = ((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3))
    u2 = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1))
    u = u1 / u2

    x = x1 + (u * (x2 - x1))
    y = y1 + (u * (y2 - y1))

    return (int(x),int(y))

def get_points(data):
    points = []
    for face in data:
        face_x = face['faceRectangle']['left']
        face_y = face['faceRectangle']['top']
        face_w = face['faceRectangle']['width']
        face_h = face['faceRectangle']['height']
        mouth_x1 = int(face['faceLandmarks']['mouthLeft']['x'])
        mouth_y1 = int(face['faceLandmarks']['mouthLeft']['y'])
        mouth_x2 = int(face['faceLandmarks']['mouthRight']['x'])
        mouth_y2 = int(face['faceLandmarks']['mouthRight']['y'])
        lips_x1 = int(face['faceLandmarks']['upperLipTop']['x'])
        lips_y1 = int(face['faceLandmarks']['upperLipTop']['y'])
        lips_x2 = int(face['faceLandmarks']['underLipBottom']['x'])
        lips_y2 = int(face['faceLandmarks']['underLipBottom']['y'])
        eyes_x1 = int((face['faceLandmarks']['eyeLeftOuter']['x'] + face['faceLandmarks']['eyeLeftInner']['x']) / 2)
        eyes_y1 = int((face['faceLandmarks']['eyeLeftOuter']['y'] + face['faceLandmarks']['eyeLeftInner']['y']) / 2)
        eyes_x2 = int((face['faceLandmarks']['eyeRightOuter']['x'] + face['faceLandmarks']['eyeRightInner']['x']) / 2)
        eyes_y2 = int((face['faceLandmarks']['eyeRightOuter']['y'] + face['faceLandmarks']['eyeRightInner']['y']) / 2)

        centre = intersect((mouth_x1, mouth_y1), (mouth_x2, mouth_y2), (lips_x1, lips_y1), (lips_x2, lips_y2))
        openness = (np.sqrt((lips_x2 - lips_x1)**2 + (lips_y2 - lips_y1)**2) * 100) / face_h
        ipd = np.sqrt((eyes_x2 - eyes_x1)**2 + (eyes_y2 - eyes_y1)**2)

        points_i = dict(face=[(face_x, face_y), (face_x+face_w, face_y+face_h)],
                mouth=[(mouth_x1, mouth_y1), (mouth_x2, mouth_y2)],
                lips=[(lips_x1, lips_y1), (lips_x2, lips_y2)],
                eyes=[(eyes_x1, eyes_y1), (eyes_x2, eyes_y2)],
                centre=centre,
                openness=openness,
                ipd=ipd)
        points.append(points_i)
    return points

def draw_rects(img, data):
    points = get_points(data)
    img2 = img
    for i in range(len(data)):
        # Draw rectangle around face
        img2 = cv2.rectangle(img2, points[i]['face'][0], points[i]['face'][1], (255,0,0))

        # Draw horizontal mouth line
        img2 = cv2.line(img2, points[i]['mouth'][0], points[i]['mouth'][1], (0,255,0))

        # Draw vertical mouth line
        img2 = cv2.line(img2, points[i]['lips'][0], points[i]['lips'][1], (0,255,0))

        # Draw eye line
        img2 = cv2.line(img2, points[i]['eyes'][0], points[i]['eyes'][1], (0,255,0))

        # Draw point at centre of mouth
        img2 = cv2.circle(img2, points[i]['centre'], 5, (0,255,0), thickness=-1)

        # Draw "openness" of mouth as number
        img2 = cv2.putText(img2, str(points[i]['openness']), points[i]['face'][0], cv2.FONT_HERSHEY_PLAIN, 1.0, (255,0,0))

        # Draw ipd as number
        img2 = cv2.putText(img2, str(points[i]['ipd']), (points[i]['face'][0][0], points[i]['face'][1][1]+12), cv2.FONT_HERSHEY_PLAIN, 1.0, (255,0,0))

    return img2

while True:
    img = take_picture()
    img_str = cv2.imencode('.jpg', img)[1].tostring()
    data = request_face(img_str)
    if data is not None:
        print(data)
        if len(data) > 0:
            print(get_points(data)[0]['ipd'])
        img2 = draw_rects(img, data)
        plt.imshow(img2)
        plt.show()
    #input("Press enter to take a picture.")


